#!/bin/bash
set -eu

# Determine block device to install Arch Linux to.
DEFAULT_BLOCK_DEVICE="/dev/$(lsblk -d -n -oNAME,RO | grep '0$' | awk {'print $1'})"
echo -n "Installation block device (defaults to $DEFAULT_BLOCK_DEVICE): "
read BLOCK_DEVICE
echo

HOSTNAME="archbox"
LOCALE="en_US.UTF-8"
KEYMAP="en-us"
USER_NAME="ckrohn"

# Ask for a passphrase to encrypt the root partition
echo -n "Enter root password: "
read -s PASSWORD
echo
echo -n "Repeat password: "
read -s password2
echo

[[ "$PASSWORD" == "$password2" ]] || ( echo "Passwords did not match"; exit 1; )

# NVME-disk numbers are prefixed with "p"
PART_NUMBER_PREFIX=""
if [[ "$BLOCK_DEVICE" == *"nvme"* ]]; then
  PART_NUMBER_PREFIX="p"
fi

# Calculate partition names
PART_EFI="${BLOCK_DEVICE}${PART_NUMBER_PREFIX}1"
PARTITION_ROOT="${BLOCK_DEVICE}${PART_NUMBER_PREFIX}2"

# Check if using EFI boot
ls /sys/firmware/efi/efivars

# Unmount boot partition
if grep -qs '/mnt/boot ' /proc/mounts; then
  umount /mnt/boot
fi

# Unmount root partition
if grep -qs '/mnt ' /proc/mounts; then
  umount /mnt
fi

# Close luks volume if opened
if [ -e /dev/mapper/cryptroot ]; then
  cryptsetup close cryptroot
fi

parted \
  -a optimal \
  -s "$BLOCK_DEVICE" \
    mklabel gpt \
    mkpart primary fat32 0% 512MiB set 1 esp on \
    mkpart primary ext4 512MiB 100%

# Setup root partition
echo -n "$PASSWORD" \
  |cryptsetup -y -v luksFormat "$PARTITION_ROOT"
echo -n "$PASSWORD" \
  |cryptsetup open "$PARTITION_ROOT" cryptroot
sleep 5
mkfs.ext4 -L ROOT /dev/mapper/cryptroot
mount -L ROOT /mnt

# Setup EFI boot partition
mkdir -p /mnt/boot
mkfs.fat -F32 "$PART_EFI"
mount "$PART_EFI" /mnt/boot

# Install base system 
pacstrap /mnt \
  amd-ucode \
  base \
  base-devel \
  bash-completion \
  dosfstools \
  efibootmgr \
  git \
  gptfdisk \
  linux \
  linux-firmware \
  mkinitcpio \
  net-tools \
  networkmanager \
  openssh \
  pacman-contrib \
  sudo \
  vim \
  wpa_supplicant

# Generate fstab with part labels
genfstab -Lp /mnt >/mnt/etc/fstab

# Set hostname
echo "$HOSTNAME" >/mnt/etc/hostname

# Configure locale
sed -i "s/#${LOCALE}/${LOCALE}/" /mnt/etc/locale.gen
arch-chroot /mnt locale-gen
arch-chroot /mnt ln -sf /usr/share/zoneinfo/Europe/Berlin /etc/localtime
echo "LANG=$LOCALE" >/mnt/etc/locale.conf
echo "KEYMAP=${KEYMAP}" >/mnt/etc/vconsole.conf

# Configure /etc/hosts
cat <<-EOF >/mnt/etc/hosts
127.0.0.1	localhost
::1		localhost
127.0.1.1	${HOSTNAME}.localdomain	${HOSTNAME}
EOF

# Configure initramfs
arch-chroot /mnt sed -i \
  's/HOOKS=.*/HOOKS=(base systemd autodetect keyboard sd-vconsole modconf block sd-encrypt filesystems fsck)/' \
  /etc/mkinitcpio.conf
arch-chroot /mnt mkinitcpio -P

# Configure bootloader (systemd-boot)
arch-chroot /mnt bootctl install

# Configure systemd-boot bootloader
cat <<-EOF >/mnt/boot/loader/loader.conf
console-mode 	auto
default 	arch
editor 		no
timeout 	1
EOF

# Create bootloader entry for Arch Linux
cat <<-EOF >/mnt/boot/loader/entries/arch.conf
title    Arch Linux
linux    /vmlinuz-linux
initrd   /amd-ucode.img
initrd   /initramfs-linux.img
options  luks.name=$(blkid -s UUID -o value "$PARTITION_ROOT")=cryptroot root=/dev/mapper/cryptroot
EOF

# Enable time sync and set hardware clock
arch-chroot /mnt timedatectl set-ntp 1
arch-chroot /mnt hwclock -w

# Configure users
if ! arch-chroot /mnt id -u "$USER_NAME" >/dev/null 2>&1 ; then
  printf "${PASSWORD}\n${PASSWORD}" | arch-chroot /mnt passwd

  arch-chroot /mnt useradd \
    -mg users \
    -G wheel,storage,power,audio,video \
    -s /bin/bash \
      "$USER_NAME"

  printf "${PASSWORD}\n${PASSWORD}" | arch-chroot /mnt passwd "$USER_NAME"
fi

# Configure sudoers file
arch-chroot /mnt sed -i 's/# %wheel ALL=(ALL) ALL/%wheel ALL=(ALL) ALL/' /etc/sudoers

# Generate ranked pacman mirror list
curl -s "https://www.archlinux.org/mirrorlist/?country=FR&country=GB&protocol=https&use_mirror_status=on" \
  |sed -e 's/^#Server/Server/' -e '/^#/d' \
  |arch-chroot /mnt rankmirrors -n 6 - \
  >/mnt/etc/pacman.d/mirrorlist

# Reboot
umount /mnt/boot
umount /mnt
reboot
